const { RandomPicture } = require('./dist/index')

;(async function () {
  const neededKeys = [
    'url',
  ]
  const random = await RandomPicture()
  const keys = Object.keys(random)
  // Test if all needed keys are present
  neededKeys.forEach(key => {
    if (!keys.includes(key)) {
      throw new Error('Random image is missing key: ' + key)
    }
  })
})()
  .then(() => {
    console.log('Tests passed');
  })
  .catch((error) => {
    console.log(error.message)
    console.error('Tests failed')
  })
