# Random picture

NPM package: https://www.npmjs.com/package/random-picture

Git repo: https://gitlab.com/ap3k/node_modules/random-picture

## Installation

With NPM

```
npm install random-picture --save
```

With Yarn

```
yarn add random-picture
```

## Usage
> Read the Source Luke!

### Get random image
```ts
const { RandomPicture } = require('random-picture')


const image = await randomPicture()
// image
interface ImageObject {
  url: string
  author?: string
  width?: number
  height?: number
}
// Example
// {
//   url: 'https://picsum.photos/id/827/3695/5543.jpg',
//   author: 'kazuend',
//   width: 3695,
//   height: 5543
// }
```
